javascript: function get_distance(o, t) {
    var o = o.split("|"),
        t = t.split("|"),
        r = t[0] - o[0],
        e = t[1] - o[1];
    return Math.sqrt(r * r + e * e);
}
var central = prompt("Zadejte souřadnice:", "0|0"),
    win = window.frames.length > 0 ? window.main : window,
    J = win.$,
    doc = window.document,
    table = J("table[id$=_table]").filter(":not(#group_table)").get(0),
    rows = [],
    q, p, rowHeight = table.rows[1].cells[0].rowSpan;
for (q = 1; q < table.tBodies[0].rows.length; q += rowHeight) {
    var row = table.tBodies[0].rows[q],
        coords = J.trim(J(row.cells[1]).text()).match(/\((\d+\|\d+)\) +K\d+$/)[1],
        rowGroup = [];
    for (p = 0; rowHeight > p; ++p) rowGroup[p] = table.tBodies[0].rows[q + p];
    rows[(q - 1) / rowHeight] = [coords, rowGroup];
}
for (rows = rows.sort(function(o, t) {
        return get_distance(o[0], central) - get_distance(t[0], central);
    }), q = 0; q < rows.length; ++q)
    for (p = 0; rowHeight > p; ++p) table.tBodies[0].appendChild(rows[q][1][p]);