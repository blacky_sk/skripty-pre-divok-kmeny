supply_rows=document.getElementsByClassName('supply_location');
if(document.location.search.indexOf('mode=call')==-1){
  UI.ErrorMessage('Skript treba spustiť v trhovisku na stránke "Vyžiadať"');
} else {
  var wood = parseInt(prompt('Zadajte počet dreva'));
  var clay = parseInt(prompt('Zadajte počet hliny'));
  var iron = parseInt(prompt('Zadajte počet železa'));
  var cur_wood = 0;
  var cur_clay = 0;
  var cur_iron = 0;
  var capacity = 0;
  var send_wood = 0;
  var send_clay = 0;
  var send_iron = 0;
  for(i=0;i<supply_rows.length;i++){
    cur_wood = parseInt(supply_rows[i].getElementsByTagName('td')[2].dataset.res);
    cur_clay = parseInt(supply_rows[i].getElementsByTagName('td')[3].dataset.res);
    cur_iron = parseInt(supply_rows[i].getElementsByTagName('td')[4].dataset.res);
    capacity = parseInt(supply_rows[i].dataset.capacity);
    send_wood = 0;
    send_clay = 0;
    send_iron = 0;

    if(wood > 0){
      if(cur_wood <= capacity){
        if(capacity >= wood){
          cur_wood = wood;
          wood = 0;
        } else {
          wood -= cur_wood;
        }
        send_wood = cur_wood;
        capacity -= send_wood;
        capacity = (~~(capacity/1000))*1000;
      } else if(cur_wood > capacity){
        if(capacity >= wood){
          send_wood = wood;
          wood = 0;
        } else {
          wood -= capacity;
          send_wood = capacity;
        }
        capacity -= send_wood;
        capacity = (~~(capacity/1000))*1000;
      }
    }
    if(clay > 0){
      if(cur_clay <= capacity){
        if(capacity >= clay){
          cur_clay = clay;
          clay = 0;
        } else {
          clay -= cur_clay;
        }
        send_clay = cur_clay;
        capacity -= send_clay;
        capacity = (~~(capacity/1000))*1000;
      } else if(cur_clay > capacity){
        if(capacity >= clay){ //keď potrebujeme menej železa než môžeme poslať
          send_clay = clay;
          clay = 0;
        } else {
          clay -= capacity;
          send_clay = capacity;
        }
        capacity -= send_clay;
        capacity = (~~(capacity/1000))*1000;
      }
    }
    if(iron > 0){ // musíme chcieť posielať nejaké železo
      if(cur_iron <= capacity){ //ak máme menej železa než vieme poslať
        if(capacity >= iron){ //keď potrebujeme menej železa než môžeme poslať
          cur_iron = iron;
          iron = 0;
        } else {
          iron -= cur_iron;
        }
        send_iron = cur_iron;
        capacity -= send_iron;
        capacity = (~~(capacity/1000))*1000;
      } else if(cur_iron > capacity){ //ak máme viac železa než vieme poslať
        if(capacity >= iron){ //keď potrebujeme menej železa než môžeme poslať
          send_iron = iron;
          iron = 0;
        } else {
          iron -= capacity;
          send_iron = capacity;
        }
        capacity -= send_iron;
        capacity = (~~(capacity/1000))*1000;
      }
    }
    supply_rows[i].getElementsByTagName('td')[7].childNodes[0].click();
    supply_rows[i].getElementsByTagName('td')[2].getElementsByTagName('input')[0].value = send_wood;
    supply_rows[i].getElementsByTagName('td')[3].getElementsByTagName('input')[0].value = send_clay;
    supply_rows[i].getElementsByTagName('td')[4].getElementsByTagName('input')[0].value = send_iron;
    supply_rows[i].getElementsByTagName('td')[7].childNodes[0].click();
  };
}